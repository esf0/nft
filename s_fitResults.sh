#!/bin/bash

# Sedov Egor
# egor.sedoff@gmail.com
# 08.02.2018

# Function fits results
# $varind - number of elemnt of array in function r_fitResult.cpp

input_dir="../results_met_new_3/"
# type='16QAM'
#max_ncar=256
#max_energy=80
#vartype=2

function doFitResults() {
    temp_type='"'$type'"'
    input='"'$input_dir'"'
    root -l -q "r_fitResults.cpp($input,$temp_type,$vartype,$varind)"
}

for vartype in 1 2
do
    for type in 'QPSK' '16QAM'
    do
        if (( $vartype == 1 ))
        then
            for varind in 0 3 7
            do
                doFitResults
            done
        elif (( $vartype == 2 ))
        then
            for varind in 4 5 6 7 8 9 11
            do
                doFitResults
            done
        fi
    done
done

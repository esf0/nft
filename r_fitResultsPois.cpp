// 20.04.18
// Egor Sedov
// egor.sedoff@gmail.com

// Function fits data with Poisson distribution

#include <fstream>
#include <iostream>
#include "stdlib.h"

Double_t poissonFit(Double_t* x, Double_t* par)
{
    return par[0]*TMath::Poisson(x[0],par[1]);
}

void r_fitResultsPois(TString dir, TString type, Int_t vartype, Int_t varind)
{
    /* Number of bins */
    Int_t NBIN = 10;
    
    Int_t NCAR = 2;
    Int_t NENG = 16;
    Int_t CYCLE = 0;
    //TString type("16QAM");

    Int_t ncar[2] = {128,1024};
    //Int_t energy[13] = {2,4,8,16,32,64,128,256,512,1024,2048,4096,8192};
    //Double_t energy[21] = {3,3.2,3.4,3.6,3.8,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,6.2,6.4,6.6,6.8,7};
    Double_t energy[16] = {30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45};
    //Int_t ncar[16] = {16,32,48,64,80,96,112,128,144,160,176,192,208,224,240,256};
    //Int_t energy[20] = {1,2,3,4,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80};
    
    //Int_t vartype = 2;

    //Int_t cartype = 3;
    //Int_t energytype = 4;
    Int_t cartype;
    Int_t energytype;

    std::stringstream finrootname;
    
    if( vartype == 1 )
    {
        cartype = varind;
        finrootname << dir << type << "_" << ncar[cartype] << "_e"  <<".root" ;
        CYCLE = NENG;
    }

    if( vartype == 2 )
    {
        energytype = varind;
        finrootname << dir << type << "_c" << "_" << energy[energytype] <<".root" ;
        CYCLE = NCAR;
    }

    std::string fname;

    TTree fintree("fintree","ROOT tree with results");

    Double_t avenergy;
    Int_t numcar;
    Double_t mean;
    Double_t mean_err;
    Double_t sigma;
    Double_t sigma_err;
    Double_t constant;
    Double_t constant_err;
    Double_t chi2;

    fintree.Branch("avenergy",&avenergy,"avenergy/D");
    fintree.Branch("numcar",&numcar,"numcar/I");
    fintree.Branch("mean",&mean,"mean/D");
    fintree.Branch("mean_err",&mean_err,"mean_err/D");
    fintree.Branch("sigma",&sigma,"sigma/D");
    fintree.Branch("sigma_err",&sigma_err,"sigma_err/D");
    fintree.Branch("constant",&constant,"constant/D");
    fintree.Branch("constant_err",&constant_err,"constant_err/D");
    fintree.Branch("chi2",&chi2,"chi2/D");


    for( int i = 0; i < CYCLE; i++ )
    {
        std::stringstream rootname;
        if( vartype == 1 )
        {
            rootname << dir << type << "_" << ncar[cartype] << "_" << energy[i] <<".root" ;
            avenergy = energy[i];
            numcar = ncar[cartype];
        }
        if( vartype == 2 )
        {
            rootname << dir << type << "_" << ncar[i] << "_" << energy[energytype] <<".root" ;
            avenergy = energy[energytype];
            numcar = ncar[i];
        }
        fname = rootname.str();
        TFile* file = TFile::Open( fname.c_str() );
        TTree* tree; file->GetObject("tree;1",tree);

        
        TCanvas* c1;
        c1 = new TCanvas("c1","c1",1600,1200);
            
        TString sdraw;
        sdraw.Form("nsolit");
        TString sdrawto;
        sdrawto.Form(" >> h_res(%d,%d,%d)",NBIN,0,NBIN);

        tree->Draw(sdraw + sdrawto,"","goff"); //goff works? Stop draw in fitf
        TH1F* h_res = (TH1F*) gDirectory->Get("h_res");
        
        Double_t norm = h_res->GetEntries();
        Double_t d_mean = h_res->GetMean();
        h_res->Scale(1/norm);
         
        TF1* f_pois = new TF1("f_pois",poissonFit,0,NBIN,2);
        f_pois->SetParName(0,"Const");
        f_pois->SetParName(1,"#mu");
        f_pois->SetLineColor(kRed);
        f_pois->SetLineWidth(4);
        f_pois->SetParameter(0,1);
        f_pois->SetParameter(1,0.5);
        //f_pois->SetParLimits(1,0.9*d_mean,1.1*d_mean);
        //f_pois->SetParameter(2,10);

        h_res->Fit("f_pois","MQR0");

        Double_t par[2];
        f_pois->GetParameters(&par[0]);
        chi2 = f_pois->GetChisquare();

        Double_t err[2];
        err[0] = f_pois->GetParError(0);
        err[1] = f_pois->GetParError(1);
        //err[2] = f_pois->GetParError(2);
        
        constant = par[0];
        constant_err = err[0];
        if( d_mean == 0 )
        {
            mean = d_mean;
            mean_err = 0;
        }
        else
        {
            mean = par[1];
            mean_err = err[1];
        }
        sigma = par[1];
        sigma_err = err[1];
        
        /* ----------- */
        
        h_res->SetTitle("");
        h_res->SetLineColor(kBlue+3);
        h_res->SetFillColor(kBlue+2);
        h_res->SetFillStyle(3003);
        h_res->SetLineWidth(3);
        h_res->Draw("HIST");

        h_res->GetXaxis()->SetTitle("Solitons number");
        h_res->GetYaxis()->SetTitle("Probability");
        h_res->GetYaxis()->SetTitleOffset(0.8);
        h_res->GetXaxis()->SetTitleSize(0.05);
        h_res->GetYaxis()->SetTitleSize(0.05);
        h_res->GetXaxis()->SetLabelSize(0.05);
        h_res->GetYaxis()->SetLabelSize(0.05);

        f_pois->Draw("same");

        TLegend* leg = new TLegend(0.65,0.70,0.98,0.95);
    //  leg->SetHeader("Legend");
        //leg->SetBorderSize(0);
        leg->SetTextSize(.04);
        leg->AddEntry(h_res,"Distribution","f");
        leg->AddEntry("f_pois","Poisson fit","l");

        TString meanstr;
        meanstr.Form("Mean %.2f",mean);
        TString fwhmstr;
        Double_t fwhm = 2*sqrt(2*log(2))*sigma;
        fwhmstr.Form("FWHM %.2f",fwhm);
        
        leg->AddEntry((TObject*)0,meanstr,"");
        leg->AddEntry((TObject*)0,fwhmstr,"");
        leg->Draw("same");

        //TLegend* p662 = new TLegend(0.73,0.42,0.74,0.43);
        //p662->AddEntry((TObject*)0,"661.7 KeV","");
        //p662->SetBorderSize(0);
        //p662->SetTextSize(.05);
        //p662->Draw("same");

        /* ----------- */
        
       
        std::stringstream setname;
        //setname << dir << type << "_" << ncar[cartype] << "_" << energy[i];

        //setname.str();

        //TString setname;
        if( vartype == 1 )
        {
            setname << dir << type << "_" << ncar[cartype] << "_" << energy[i] << ".pdf";
            //setname.Form("../results_met_new/qpsk_%d_%i.pdf",ncar[cartype],energy[i]);
        }
        if( vartype == 2 )
        {
            setname << dir << type << "_" << ncar[i] << "_" << energy[energytype] << ".pdf";
            //setname.Form("../results_met_new/qpsk_%d_%i.pdf",ncar[i],energy[energytype]);
        }
        c1->Print(setname.str().c_str());
        sleep(1);

        delete f_pois;
        delete h_res;
        delete c1;
        
        fintree.Fill();
    }
    
    fname = finrootname.str();

    TFile* f = new TFile( fname.c_str(), "recreate" );
    fintree.Write();
    f->Close();
    
    return 0;
}

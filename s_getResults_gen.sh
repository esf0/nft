#!/bin/bash

# Sedov Egor
# egor.sedoff@gmail.com
# 08.02.2018

input_dir='../results_met_new_3/'
# type='16QAM'
max_ncar=256
max_energy=80
vartype=1

function doGetResults() {
    input='"'"${input_dir}${type}_${ncar}_${energy}.txt"'"'
    temp_type='"'$type'"'
    outdir='"'$input_dir'"'
    root -l -q "r_getResults.cpp($input,$outdir,$temp_type,$ncar,$energy)"
}

for type in 'QPSK' '16QAM'
do
if (( $vartype == 1 ))
then
    for ncar in 16 64 128
    do
        for energy in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192
        do
            doGetResults
            let "energy = energy + 1"
        done
    done
elif (( $vartype == 2 ))
then
    for energy in 5 10 15 20 25 30 40
#    for energy in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192
    do
        ncar=16
        until (( $ncar > $max_ncar ))
        do
            doGetResults
            let "ncar = ncar + 16"
        done
    done
fi
done

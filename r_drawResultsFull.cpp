// Egor Sedov
// 16.04.2018
// egor.sedoff@gmail.com

// This program trace graphs (Soliton numbers depend on number of subcarriers) for $NIN values of energy

#include <fstream>
#include <iostream>
#include "stdlib.h"


void r_drawResultsFull()
{
    Int_t range = 120;

    TString dir("../results_met_new_3/");
    TString type("16QAM");

    Int_t NCAR = 16;
    Int_t NENG = 7;

    Int_t energy[7] = {5,10,15,20,25,30,40};
    Int_t ncar[16] = {16,32,48,64,80,96,112,128,144,160,176,192,208,224,240,256};
    Int_t NIN = 7;
    TGraphErrors* gre[7];

    Int_t vartype = 2;

    if ( vartype != 1 && vartype != 2 )
    {
        std::cout << "Type should be 1 for energy and 2 gor carriers " << std::endl;
        return -1;
    }
        
    Double_t xm[256][7];
    Double_t ym[256][7];
    Double_t yem[256][7];

    Int_t numev[7];
    
    for( Int_t in = 0 ; in < NIN; in++ )
    {
        std::stringstream inroot;

        inroot << dir << type << "_c_" << energy[in] << ".root" ;
        
        TFile* file = TFile::Open( inroot.str().c_str() );
        TTree* tree; file->GetObject("fintree;1",tree);

        auto nevent = tree->GetEntries();

        Double_t mean;
        Double_t mean_err;
        Double_t avenergy;
        Int_t numcar;
    
        tree->SetBranchAddress("mean",&mean);
        tree->SetBranchAddress("mean_err",&mean_err);
        if ( vartype == 1 )
            tree->SetBranchAddress("avenergy",&avenergy);
        if ( vartype == 2 )
            tree->SetBranchAddress("numcar",&numcar);
    
        Double_t x[256];
        Double_t y[256];
        Double_t ye[256];



        Int_t N0 = 576; 
        Double_t energy_const = 1;
        //Double_t energy_const = 21.5/(1.27*0.001)/(2200*2200*N0*N0);
        for( Int_t i = 0; i < nevent; i++ )
        {
            tree->GetEntry(i);
            if ( vartype == 1 )
                x[i] = avenergy*energy_const;
            if ( vartype == 2 )
                x[i] = numcar;
            y[i] = mean;
            ye[i] = mean_err;
            
            numev[in] = nevent;
            xm[i][in] = x[i];
            ym[i][in] = y[i];
            yem[i][in] = ye[i];
            std::cout << xm[i][in] << " " << ym[i][in] << " " << yem[i][in] << std::endl;
        }

    
        file->Close();
    }
    
    Double_t x[256];
    Double_t y[256];
    Double_t ye[256];
    
    //TGraphErrors* gr[7];
    for( Int_t i = 0; i < NIN; i++ )
    {
        for( Int_t j = 0; j < numev[i]; j++ )
        {
            x[j] = xm[j][i];
            y[j] = ym[j][i];
            ye[j] = yem[j][i];
        }
        gre[i] = new TGraphErrors(numev[i],x,y, NULL , ye); // Create graph with errors
    }
     
    gre[0]->SetName("gr0");
    gre[0]->SetTitle("0.65");
    gre[1]->SetName("gr1");
    gre[1]->SetTitle("1.30");
    gre[2]->SetName("gr2");
    gre[2]->SetTitle("1.95");
    gre[3]->SetName("gr3");
    gre[3]->SetTitle("2.60");
    gre[4]->SetName("gr4");
    gre[4]->SetTitle("3.25");
    gre[5]->SetName("gr5");
    gre[5]->SetTitle("3.90");
    gre[6]->SetName("gr6");
    gre[6]->SetTitle("5.20");
        
    for( Int_t i = 0; i < NIN; i++ )
    {  
        gre[i]->SetMarkerStyle(20);
        gre[i]->SetMarkerSize(1.5);
        gre[i]->SetDrawOption("APL");
        gre[i]->SetLineWidth(2);
        gre[i]->SetFillStyle(0);
    }
    gre[0]->SetMarkerColor(kBlue);
    gre[0]->SetLineColor(kBlue);
    gre[1]->SetMarkerColor(kGreen);
    gre[1]->SetLineColor(kGreen);
    gre[2]->SetMarkerColor(kViolet);
    gre[2]->SetLineColor(kViolet);
    gre[3]->SetMarkerColor(kBlue+2);
    gre[3]->SetLineColor(kBlue+2);
    gre[3]->SetLineStyle(7);
    gre[4]->SetMarkerColor(kGreen+2);
    gre[4]->SetLineColor(kGreen+2);
    gre[5]->SetMarkerColor(kViolet-6);
    gre[5]->SetMarkerStyle(22);
    gre[5]->SetLineColor(kViolet-6);
    gre[6]->SetMarkerColor(kAzure+8);
    gre[6]->SetLineColor(kAzure+8);
    
    
    auto mg = new TMultiGraph("mg","");
    for( Int_t i = 0; i < NIN; i++ )
        mg->Add( gre[i] );

/* --------- Stop here */

    auto c1 = new TCanvas("c1","c1",1600,1200);
    c1->SetGrid();
    
    mg->Draw("APL");

    

    /*
    TString sdraw;
    TString sedraw;
    TString sdrawto;
    TString sedrawto;

    sdraw.Form("mean:avenergy");
    sedraw.Form("mean_err:avenergy");
    sdrawto.Form(" >> h_mean(10)");
    sedrawto.Form(" >> h_meane(10)");
    

    tree->Draw(sdraw + sdrawto,"","goff");
    //tree->Draw(sdraw + sdrawto);
    TH2D* h_mean = (TH2D*) gDirectory->Get("h_mean");
    TGraph gr1(h_mean->ProfileX());

    tree->Draw(sedraw + sedrawto,"","goff");
    //tree->Draw(sedraw + sedrawto);
    TH2D* h_meane = (TH2D*) gDirectory->Get("h_meane");
    TGraph gr2(h_meane->ProfileX());
    
    */
    //TGraphErrors* gr = new TGraphErrors(10, gr1.GetX(), gr1.GetY(), NULL , gr2.GetY()); // Create graph with errors


    //gr->SetMarkerStyle(23);                 
    //gr->SetMarkerColor(kBlue);                 
    //gr->SetMarkerSize(2);
    //gr->SetLineWidth(2);
    //gr->SetTitle("");
//  gr->GetYaxis()->SetRangeUser(1700,1800);
    mg->GetYaxis()->SetRangeUser(0,range);
    mg->GetYaxis()->SetTitle("Number of solitons");
    mg->GetYaxis()->SetTitleOffset(0.95);
    if ( vartype == 1 )
        mg->GetXaxis()->SetTitle("Signal energy, dBm");
    if ( vartype == 2 )
        mg->GetXaxis()->SetTitle("Number of subcarriers");
    mg->GetYaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetLabelSize(0.05);
    mg->GetYaxis()->SetLabelSize(0.05);
//  gr->SetMarkerColor(6);
    
//    auto legend = c1->BuildLegend(0.15,0.95,0.25,0.55,"L1 [ns mW^{1/2}]");

    auto legend = new TLegend(0.15,1,0.5,0.65);
    legend->SetNColumns(2);

    legend->SetHeader("Value of L1 norm [ns mW^{1/2}]","C");
    legend->AddEntry("gr0","0.65","lp");
    legend->AddEntry("gr1","1.30","lp");
    legend->AddEntry("gr2","1.95","lp");
    legend->AddEntry("gr3","2.60","lp");
    legend->AddEntry("gr4","3.25","lp");
    legend->AddEntry("gr5","3.90","lp");
    legend->AddEntry("gr6","5.20","lp");
    
    legend->Draw("SAME");

    //mg->Draw("APL");

    //gPad->SetGrid(1,1);

    //gre = gr;
    //gre->Draw("APL*");
    std::stringstream outpdf;

    outpdf << dir << type << "_c.pdf" ;
   
    c1->Print(outpdf.str().c_str()); 
    
    
    return 0;
}

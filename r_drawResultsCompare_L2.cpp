#include <fstream>
#include <iostream>
#include "stdlib.h"
#include "math.h"

void r_drawResultsCompare_L2()
{
    Int_t range = 5;

    TString dir("../results_en_1/");
    //TString type("16QAM");

    Int_t NCAR = 2;
    Int_t NENG = 4;

    //Int_t energy[4] = {1024,2048,4096,8192};
    Int_t ncar[2] = {128,1024};
    Int_t NIN = 4;
    TGraphErrors* gre[4];

    Int_t vartype = 1;

    if ( vartype != 1 && vartype != 2 )
    {
        std::cout << "Type should be 1 for energy and 2 gor carriers " << std::endl;
        return -1;
    }
        
    Double_t xm[256][8];
    Double_t ym[256][8];
    Double_t yem[256][8];

    Int_t numev[8];
    
    for( Int_t in = 0 ; in < NIN; in++ )
    {
        std::stringstream inroot;
        
        if( in >= 2 )
        {
            TString type("16QAM");
            inroot << dir << type << "_" << ncar[in-2] << "_e" << ".root" ;
        }
        else
        {
            TString type("QPSK");
            inroot << dir << type << "_" << ncar[in] << "_e" << ".root" ;
        }
        //inroot << dir << type << "_c_" << energy[in] << ".root" ;
        
        TFile* file = TFile::Open( inroot.str().c_str() );
        TTree* tree; file->GetObject("fintree;1",tree);

        auto nevent = tree->GetEntries();

        Double_t mean;
        Double_t mean_err;
        Double_t avenergy;
        Int_t numcar;
    
        tree->SetBranchAddress("mean",&mean);
        tree->SetBranchAddress("mean_err",&mean_err);
        if ( vartype == 1 )
            tree->SetBranchAddress("avenergy",&avenergy);
        if ( vartype == 2 )
            tree->SetBranchAddress("numcar",&numcar);
    
        Double_t x[256];
        Double_t y[256];
        Double_t ye[256];



        Int_t T0 = 10000; 
        //Double_t energy_const = 1;
        Double_t energy_const = 21.5/(1.27*0.001)/(T0*T0);
        for( Int_t i = 0; i < nevent; i++ )
        {
            tree->GetEntry(i);
            if ( vartype == 1 )
                x[i] = 10*log10(avenergy*energy_const);
            if ( vartype == 2 )
                x[i] = numcar;
            y[i] = mean;
            ye[i] = mean_err;
            
            numev[in] = nevent;
            xm[i][in] = x[i];
            ym[i][in] = y[i];
            yem[i][in] = ye[i];
            std::cout << xm[i][in] << " " << ym[i][in] << " " << yem[i][in] << std::endl;
        }

    
        file->Close();
    }
    
    Double_t x[256];
    Double_t y[256];
    Double_t ye[256];
    
    //TGraphErrors* gr[7];
    for( Int_t i = 0; i < NIN; i++ )
    {
        for( Int_t j = 0; j < numev[i]; j++ )
        {
            x[j] = xm[j][i];
            y[j] = ym[j][i];
            ye[j] = yem[j][i];
        }
        gre[i] = new TGraphErrors(numev[i],x,y, NULL , ye); // Create graph with errors
    }
     
    gre[0]->SetName("gr0");
    gre[0]->SetTitle("128");
    gre[1]->SetName("gr1");
    gre[1]->SetTitle("1024");
    gre[2]->SetName("gr2");
    gre[2]->SetTitle("128");
    gre[3]->SetName("gr3");
    gre[3]->SetTitle("1024");
        
    for( Int_t i = 0; i < NIN; i++ )
    {  
        gre[i]->SetMarkerStyle(20);
        gre[i]->SetMarkerSize(1.5);
        gre[i]->SetDrawOption("APL");
        gre[i]->SetLineWidth(2);
        gre[i]->SetFillStyle(0);
    }
    
    gre[0]->SetMarkerColor(kGreen);
    gre[0]->SetLineColor(kGreen);
    gre[2]->SetMarkerColor(kGreen+3);
    gre[2]->SetLineColor(kGreen+3);
    gre[2]->SetLineStyle(5);
    
    gre[1]->SetMarkerColor(kViolet);
    gre[1]->SetLineColor(kViolet);
    gre[3]->SetMarkerColor(kViolet-6);
    gre[3]->SetLineColor(kViolet-6);
    gre[3]->SetLineStyle(5);
    
    
    auto mg1 = new TMultiGraph("mg1","");
    auto mg2 = new TMultiGraph("mg2","");
    mg1->Add( gre[0] );
    mg1->Add( gre[2] );
    mg2->Add( gre[1] );
    mg2->Add( gre[3] );

/* --------- Stop here */

    auto c1 = new TCanvas("c1","c1",1600,1200);
    
    auto pad2 = new TPad("pad2","pad1title",0,0,0.50,1);
    auto pad1 = new TPad("pad1","pad2title",0.50,0,1,1);

    pad1->Draw();
    pad2->Draw();

    pad1->cd();
    pad1->SetGridx();
    pad1->SetGridy();
    pad1->SetRightMargin(0.1);
    pad1->SetLeftMargin(0);
    pad1->SetTicks(0,2);

    mg1->Draw("APL");



    pad2->cd();
    pad2->SetGridx();
    pad2->SetGridy();
    pad2->SetRightMargin(0);
    pad2->SetLeftMargin(0.1);

    mg2->Draw("APL");

    c1->Update();
    //c1->cd();
    //c1->SetGrid();
    
    //mg->Draw("APL");


    /*
    TString sdraw;
    TString sedraw;
    TString sdrawto;
    TString sedrawto;

    sdraw.Form("mean:avenergy");
    sedraw.Form("mean_err:avenergy");
    sdrawto.Form(" >> h_mean(10)");
    sedrawto.Form(" >> h_meane(10)");
    

    tree->Draw(sdraw + sdrawto,"","goff");
    //tree->Draw(sdraw + sdrawto);
    TH2D* h_mean = (TH2D*) gDirectory->Get("h_mean");
    TGraph gr1(h_mean->ProfileX());

    tree->Draw(sedraw + sedrawto,"","goff");
    //tree->Draw(sedraw + sedrawto);
    TH2D* h_meane = (TH2D*) gDirectory->Get("h_meane");
    TGraph gr2(h_meane->ProfileX());
    
    */
    //TGraphErrors* gr = new TGraphErrors(10, gr1.GetX(), gr1.GetY(), NULL , gr2.GetY()); // Create graph with errors


    //gr->SetMarkerStyle(23);                 
    //gr->SetMarkerColor(kBlue);                 
    //gr->SetMarkerSize(2);
    //gr->SetLineWidth(2);
    //gr->SetTitle("");
//  gr->GetYaxis()->SetRangeUser(1700,1800);

    mg1->GetYaxis()->SetRangeUser(0,range);
    mg2->GetYaxis()->SetRangeUser(0,range);
/*    mg->GetYaxis()->SetRangeUser(0,range);
    mg->GetYaxis()->SetTitle("Probability of soliton emerging");
    mg->GetYaxis()->SetTitleOffset(0.95);
    if ( vartype == 1 )
        mg->GetXaxis()->SetTitle("Signal energy, dBm");
    if ( vartype == 2 )
        mg->GetXaxis()->SetTitle("Number of subcarriers");
    mg->GetYaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetLabelSize(0.05);
    mg->GetYaxis()->SetLabelSize(0.05);
*/
//  gr->SetMarkerColor(6);
    
//    auto legend = c1->BuildLegend(0.15,0.95,0.25,0.55,"L1 [ns mW^{1/2}]");

    //gPad->SetLogx();
    
    auto legend = new TLegend(0.15,1,0.5,0.65);

    legend->SetHeader("1024 subcarriers","C");
    legend->AddEntry("gr1","QPSK","lp");
    legend->AddEntry("gr3","16QAM","lp");
    
    legend->Draw("SAME");

    pad1->cd();

    auto legend2 = new TLegend(0.15,1,0.5,0.65);

    legend2->SetHeader("128 subcarriers","C");
    legend2->AddEntry("gr0","QPSK","lp");
    legend2->AddEntry("gr2","16QAM","lp");
    
    legend2->Draw("SAME");


    //mg->Draw("APL");

    //gPad->SetGrid(1,1);

    //gre = gr;
    //gre->Draw("APL*");
    std::stringstream outpdf;

    outpdf << dir << "compare_e_l2.pdf" ;
   
    c1->Print(outpdf.str().c_str()); 
    
    
    return 0;
}

#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TCut.h"
#include "TGraph.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
/*
TChain* createChain(Int_t date[], Int_t n)
{
    TChain* chain = new TChain("fintree");

    for( Int_t i = 0; i < n; i++ )
    {
        Int_t todayis = date[i];

		std::stringstream rootname;
		rootname << "../histogram/"; 
        if( todayis / 10000 < 10 )
            rootname << "0";
        rootname << todayis / 10000 << "." 
			<< (todayis / 100) % 100 << "."
			<< todayis % 100 << "/csi" << "_"
			<< todayis << ".root";
	   
        std::string rname = rootname.str();
        chain->Add(rname);

        char str[100];
        if( todayis / 10000 < 10 )
            sprintf(str,"../histogram/0%d.%d.%d/csi_%d.root",todayis/10000,(todayis/100)%100,todayis%100,todayis);
        else
            sprintf(str,"../histogram/%d.%d.%d/csi_%d.root",todayis/10000,(todayis/100)%100,todayis%100,todayis);
        chain->Add(str);
    }

    return chain;
}
*/
int dataToRoot(Int_t date, Int_t crystal_num[], Int_t n)
{
	Int_t todayis = date;
	Int_t crystal;
	Int_t runnum;
    Double_t t;
	
	for( crystal = 80; crystal <= 110; crystal++ )
		for( int j = 0; j < n; j++ )
			if( crystal == crystal_num[j] )
	for( runnum = 1; runnum <= 11 ; runnum++ )
	{	
	
		std::stringstream tempfname;
		std::string fname;

		tempfname << "../histogram/"; 
        
        if( todayis / 10000 < 10 )
		    tempfname << "0"; 
		
        tempfname <<  todayis / 10000 << ".";

        if( ( todayis / 100 ) % 100 < 10 )
		    tempfname << "0"; 

		tempfname << (todayis / 100) % 100 << "."
		    << todayis % 100 << "/csi";
            
				if( crystal >= 101 )
				{
					if( crystal == 101 )
            tempfname << "Test1" << "_001";
					if( crystal == 102 )
            tempfname << "Test2" << "_001";
					if( crystal == 103 )
            tempfname << "Test3" << "_001";
					if( crystal == 104 )
            tempfname << "Test4" << "_001";
				}
				else
				{
					if( crystal == 100 )
						  tempfname << "ref" << "_0";
					else
							tempfname << crystal << "_0";
        
        
					if( runnum < 10 )
							tempfname << "0" << runnum;
					else
							tempfname << runnum;
				}
        tempfname << "_eh_0.dat";
		
		fname = tempfname.str();
		std::cout << fname << " try to open" << std::endl;
	
		std::ifstream file( fname.c_str() );
	
		if( !file.is_open() )
		{
			std::cout << "File couldn't be opened" << std::endl;
			continue;
		}
	
		std::stringstream rootname;
		rootname << "../histogram/"; 
        if( todayis / 10000 < 10 )
            rootname << "0";
        rootname << todayis / 10000 << ".";
        if( ( todayis / 100 ) % 100 < 10 )
            rootname << "0";
		rootname << (todayis / 100) % 100 << "."
			<< todayis % 100 << "/csi" << crystal << "_"
			<< runnum << ".root";
	    fname = rootname.str();
	
	    TFile* f = new TFile( fname.c_str(),"recreate");
	    TTree tree("tree","Experiment ROOT tree");
	
	    Double_t charge;
	    tree.Branch("todayis",&todayis,"todayis/I");
	    tree.Branch("charge",&charge,"charge/D");
	    tree.Branch("crystal",&crystal,"crystal/I");
	    tree.Branch("runnum",&runnum,"runnum/I");
	    tree.Branch("t",&t,"t/D");
	
	    // read info    
	    char buff[1000];

		while( !file.eof() )
		{
			file >> buff;	
			Double_t eventcharge = atof(buff);
			file >> buff;
			Double_t eventnum = atof(buff);
			
			if( eventcharge != 32767 )
				for( int i = 0; i < eventnum; i++ )
				{
					charge = eventcharge;
					tree.Fill();
				}
            else
            {
                break;
            }
		}

		tree.Write();
		f->Close();
		file.close();
	}
	return 0;
}

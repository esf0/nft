#include <fstream>
#include <iostream>
#include "stdlib.h"

void r_drawErrorSY()
{
    TString dir("../results_met_new_3/");
    Int_t range = 120;

    TGraphErrors* gre[7];
    
    Double_t x[256];
    Double_t y[256];
    
    x[0] = 100; x[1] = 200; x[2] = 300; x[3] = 400;
    
    y[0] = 0.003148342879299; y[1] = 0.000003201696626; y[2] = 0.000000000942196; y[3] = 0.000000000000165;
    gre[0] = new TGraphErrors(4,x,y, NULL , NULL);
    
    y[0] = 0.042342467292874; y[1] = 0.000052478332335; y[2] = 0.000000016540089; y[3] = 0.000000000003057;
    gre[1] = new TGraphErrors(4,x,y, NULL , NULL);
    
    y[0] = 0.360037135648836; y[1] = 0.000431510329800; y[2] = 0.000000114314784; y[3] = 0.000000018209988;
    gre[2] = new TGraphErrors(4,x,y, NULL , NULL);
    
     
    gre[0]->SetName("gr0");
    gre[0]->SetTitle("2.2i");
    gre[1]->SetName("gr1");
    gre[1]->SetTitle("1.2i");
    gre[2]->SetName("gr2");
    gre[2]->SetTitle("0.2i");
        
    for( Int_t i = 0; i < 3; i++ )
    {  
        gre[i]->SetMarkerStyle(20+i);
        gre[i]->SetMarkerSize(3);
        gre[i]->SetDrawOption("APL");
        gre[i]->SetLineWidth(3);
        gre[i]->SetFillStyle(0);
    }
    gre[0]->SetMarkerColor(kBlue);
    gre[0]->SetLineColor(kBlue);
    gre[1]->SetMarkerColor(kGreen);
    gre[1]->SetLineColor(kGreen);
    gre[2]->SetMarkerColor(kViolet);
    gre[2]->SetLineColor(kViolet);
    
    
    auto mg = new TMultiGraph("mg","");
    for( Int_t i = 0; i < 3; i++ )
        mg->Add( gre[i] );

/* --------- Stop here */

    auto c1 = new TCanvas("c1","c1",1600,1200);
    c1->SetGrid();
    
    mg->Draw("APL");

    gPad->SetLogy();
    //mg->GetYaxis()->SetRangeUser(0,range);
    //mg->GetYaxis()->CenterTitle();
    mg->GetYaxis()->SetNdivisions(505);
    //mg->GetYaxis()->SetTitle("Relative error");
    mg->GetYaxis()->SetTitleOffset(0.95);
    mg->GetXaxis()->SetTitle("Number of discretization points");
    mg->GetYaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetLabelSize(0.05);
    mg->GetYaxis()->SetLabelSize(0.05);
//  gr->SetMarkerColor(6);
    
    auto legend = new TLegend(0.65,0.95,0.9,0.65);
    legend->SetNColumns(2);

    legend->SetHeader("Eigenvalues","C");
    legend->AddEntry("gr0","2.2i","lp");
    legend->AddEntry("gr1","1.2i","lp");
    legend->AddEntry("gr2","0.2i","lp");
    
    legend->Draw("SAME");

    std::stringstream outpdf;
    outpdf << dir << "SY_signal.pdf" ;
   
    c1->Print(outpdf.str().c_str()); 
    
    
    return 0;
}

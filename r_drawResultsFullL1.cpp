// Egor Sedov
// 16.04.2018
// egor.sedoff@gmail.com

// Function trace number of appearing solitons depending on value of L1 norm
// for $NIN = $NCAR different values of subcarriers

#include <fstream>
#include <iostream>
#include "stdlib.h"

void r_drawResultsFullEn()
{
    Int_t range = 80;

    TString dir("../results_met_new_3/");
    TString type("16QAM");

    Int_t NCAR = 3;
    Int_t NENG = 7;

    Int_t energy[7] = {5,10,15,20,25,30,40};
    Int_t ncar[3] = {16,64,128};
    Int_t NIN = 3;
    TGraphErrors* gre[3];

    Int_t vartype = 1;

    if ( vartype != 1 && vartype != 2 )
    {
        std::cout << "Type should be 1 for energy and 2 gor carriers " << std::endl;
        return -1;
    }
        
    Double_t xm[256][7];
    Double_t ym[256][7];
    Double_t yem[256][7];

    Int_t numev[7];
    
    for( Int_t in = 0 ; in < NIN; in++ )
    {
        std::stringstream inroot;

        inroot << dir << type << "_" << ncar[in] << "_e" << ".root" ;
        
        TFile* file = TFile::Open( inroot.str().c_str() );
        TTree* tree; file->GetObject("fintree;1",tree);

        auto nevent = tree->GetEntries();

        Double_t mean;
        Double_t mean_err;
        Double_t avenergy;
        Int_t numcar;
    
        tree->SetBranchAddress("mean",&mean);
        tree->SetBranchAddress("mean_err",&mean_err);
        if ( vartype == 1 )
            tree->SetBranchAddress("avenergy",&avenergy);
        if ( vartype == 2 )
            tree->SetBranchAddress("numcar",&numcar);
    
        Double_t x[256];
        Double_t y[256];
        Double_t ye[256];



        Int_t N0 = 576; 
        Double_t energy_const = 0.13;
        //Double_t energy_const = 21.5/(1.27*0.001)/(2200*2200*N0*N0);
        for( Int_t i = 0; i < nevent; i++ )
        {
            tree->GetEntry(i);
            if ( vartype == 1 )
                x[i] = avenergy*energy_const;
            if ( vartype == 2 )
                x[i] = numcar;
            y[i] = mean;
            ye[i] = mean_err;
            
            numev[in] = nevent;
            xm[i][in] = x[i];
            ym[i][in] = y[i];
            yem[i][in] = ye[i];
            std::cout << xm[i][in] << " " << ym[i][in] << " " << yem[i][in] << std::endl;
        }

    
        file->Close();
    }
    
    Double_t x[256];
    Double_t y[256];
    Double_t ye[256];
    
    //TGraphErrors* gr[7];
    for( Int_t i = 0; i < NIN; i++ )
    {
        for( Int_t j = 0; j < numev[i]; j++ )
        {
            x[j] = xm[j][i];
            y[j] = ym[j][i];
            ye[j] = yem[j][i];
        }
        gre[i] = new TGraphErrors(numev[i],x,y, NULL , ye); // Create graph with errors
    }
     
    gre[0]->SetName("gr0");
    gre[0]->SetTitle("16 carriers");
    gre[1]->SetName("gr1");
    gre[1]->SetTitle("64 carriers");
    gre[2]->SetName("gr2");
    gre[2]->SetTitle("128 carriers");
    
    for( Int_t i = 0; i < NIN; i++ )
    {  
        if( i < 5 )
            gre[i]->SetMarkerStyle(20+i);
        if( i >= 5 )
            gre[i]->SetMarkerStyle(28+i);

        //if( i == NIN )
        //    gre[i]->SetMarkerStyle(33);
            
        gre[i]->SetMarkerSize(2);
        //gre[i]->SetMarkerColor(i+2);
        gre[i]->SetDrawOption("APL");
        gre[i]->SetLineColor(i+2);
        gre[i]->SetLineWidth(3);
        gre[i]->SetFillStyle(0);
    }
    
    auto mg = new TMultiGraph("mg","");
    for( Int_t i = 0; i < NIN; i++ )
        mg->Add( gre[i] );

/* --------- Stop here */

    auto c1 = new TCanvas("c1","c1",1600,1200);
    c1->SetGrid();
    
    mg->Draw("APL");

    

    /*
    TString sdraw;
    TString sedraw;
    TString sdrawto;
    TString sedrawto;

    sdraw.Form("mean:avenergy");
    sedraw.Form("mean_err:avenergy");
    sdrawto.Form(" >> h_mean(10)");
    sedrawto.Form(" >> h_meane(10)");
    

    tree->Draw(sdraw + sdrawto,"","goff");
    //tree->Draw(sdraw + sdrawto);
    TH2D* h_mean = (TH2D*) gDirectory->Get("h_mean");
    TGraph gr1(h_mean->ProfileX());

    tree->Draw(sedraw + sedrawto,"","goff");
    //tree->Draw(sedraw + sedrawto);
    TH2D* h_meane = (TH2D*) gDirectory->Get("h_meane");
    TGraph gr2(h_meane->ProfileX());
    
    */
    //TGraphErrors* gr = new TGraphErrors(10, gr1.GetX(), gr1.GetY(), NULL , gr2.GetY()); // Create graph with errors


    //gr->SetMarkerStyle(23);                 
    //gr->SetMarkerColor(kBlue);                 
    //gr->SetMarkerSize(2);
    //gr->SetLineWidth(2);
    //gr->SetTitle("");
//  gr->GetYaxis()->SetRangeUser(1700,1800);
    mg->GetYaxis()->SetRangeUser(0,range);
    mg->GetYaxis()->SetTitle("Number of solitons");
    mg->GetYaxis()->SetTitleOffset(0.8);
    if ( vartype == 1 )
        mg->GetXaxis()->SetTitle("Value of L1 norm, ns mW^{1/2}");
    if ( vartype == 2 )
        mg->GetXaxis()->SetTitle("Number of subcarriers");
    mg->GetYaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetLabelSize(0.05);
    mg->GetYaxis()->SetLabelSize(0.05);
//  gr->SetMarkerColor(6);
    
    //auto legend = c1->BuildLegend(0.15,0.95,0.25,0.55,"Subcarriers");
    
    auto legend = new TLegend(0.15,1,0.55,0.8);
    legend->SetNColumns(3);

    legend->SetHeader("Number of subcarriers","C");
    legend->AddEntry("gr0","16","lp");
    legend->AddEntry("gr1","64","lp");
    legend->AddEntry("gr2","128","lp");

    legend->Draw("SAME");
    //mg->Draw("APL");

    //gPad->SetGrid(1,1);

    //gre = gr;
    //gre->Draw("APL*");
    std::stringstream outpdf;

    outpdf << dir << type << "_e.pdf" ;
   
    c1->Print(outpdf.str().c_str()); 
    
    
    return 0;
}

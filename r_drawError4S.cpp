#include <fstream>
#include <iostream>
#include "stdlib.h"

void r_drawError4S()
{
    TString dir("../results_met_new_3/");
    Int_t range = 120;

    TGraphErrors* gre[7];
    
    Double_t x[256];
    Double_t y[256];
    
    x[0] = 100; x[1] = 200; x[2] = 300; x[3] = 400; x[4] = 500; x[5] = 800;
    
    y[0] = 0.739231940958915; y[1] = 0.382730414688426; y[2] = 0.051366106250091; 
    y[3] = 0.010808646558863; y[4] = 0.003385807190938; y[5] = 0.000008227575954;
    gre[0] = new TGraphErrors(6,x,y, NULL , NULL);
    
    y[0] = 1.258902886392355; y[1] = 0.097205889451848; y[2] = 0.020064233566684; 
    y[3] = 0.003972136698499; y[4] = 0.000792621858220; y[5] = 0.000003858579927;
    gre[1] = new TGraphErrors(6,x,y, NULL , NULL);
    
    y[0] = 1.610008291361001; y[1] = 0.001662875727419; y[2] = 0.002564764874279; 
    y[3] = 0.000555731544836; y[4] = 0.000084503497580; y[5] = 0.000000561617964;
    gre[2] = new TGraphErrors(6,x,y, NULL , NULL);
    
    y[0] = 0.904533364921487; y[1] = 0.207954873698733; y[2] = 0.072201144293575; 
    y[3] = 0.021371406076750; y[4] = 0.002525152013564; y[5] = 0.000018356956048;
    gre[3] = new TGraphErrors(6,x,y, NULL , NULL);
     
    gre[0]->SetName("gr0");
    gre[0]->SetTitle("1+2.2i");
    gre[1]->SetName("gr1");
    gre[1]->SetTitle("0.5+1.2i");
    gre[2]->SetName("gr2");
    gre[2]->SetTitle("-1+0.5i");
    gre[3]->SetName("gr3");
    gre[3]->SetTitle("0.2i");
        
    for( Int_t i = 0; i < 4; i++ )
    {  
        gre[i]->SetMarkerStyle(20+i);
        gre[i]->SetMarkerSize(3);
        gre[i]->SetDrawOption("APL");
        gre[i]->SetLineWidth(3);
        gre[i]->SetFillStyle(0);
    }
    gre[0]->SetMarkerColor(kBlue);
    gre[0]->SetLineColor(kBlue);
    gre[1]->SetMarkerColor(kGreen);
    gre[1]->SetLineColor(kGreen);
    gre[2]->SetMarkerColor(kViolet);
    gre[2]->SetLineColor(kViolet);
    gre[3]->SetMarkerColor(kRed);
    gre[3]->SetLineColor(kRed);
    
    
    auto mg = new TMultiGraph("mg","");
    for( Int_t i = 0; i < 4; i++ )
        mg->Add( gre[i] );

/* --------- Stop here */

    auto c1 = new TCanvas("c1","c1",1600,1200);
    c1->SetGrid();
    
    mg->Draw("APL");

    gPad->SetLogy();
    //mg->GetYaxis()->SetRangeUser(0,range);
    //mg->GetYaxis()->CenterTitle();
    mg->GetYaxis()->SetNdivisions(511);
    //mg->GetYaxis()->SetTitle("Relative error");
    mg->GetYaxis()->SetTitleOffset(0.95);
    mg->GetXaxis()->SetTitle("Number of discretization points");
    mg->GetYaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetTitleSize(0.05);
    mg->GetXaxis()->SetLabelSize(0.05);
    mg->GetYaxis()->SetLabelSize(0.05);
//  gr->SetMarkerColor(6);
    
    auto legend = new TLegend(0.65,0.95,0.9,0.65);
    legend->SetNColumns(2);

    legend->SetHeader("Eigenvalues","C");
    legend->AddEntry("gr0","1+2.2i","lp");
    legend->AddEntry("gr1","0.5+1.2i","lp");
    legend->AddEntry("gr2","-1+0.5i","lp");
    legend->AddEntry("gr3","0.2i","lp");
    
    legend->Draw("SAME");

    std::stringstream outpdf;
    outpdf << dir << "4S_signal.pdf" ;
   
    c1->Print(outpdf.str().c_str()); 
    
    
    return 0;
}

#include <fstream>
#include <iostream>
#include "stdlib.h"

void r_drawResults(TString input, TString output, Int_t type, Int_t range)
{
    if ( type != 1 && type != 2 )
    {
        std::cout << "Type should be 1 for energy and 2 gor carriers " << std::endl;
        return -1;
    }
    TFile* file = TFile::Open( input );
    TTree* tree; file->GetObject("fintree;1",tree);

    //Event* event = new Event();

    //auto mean = tree->GetBranch("mean");
    //auto mean_err = tree->GetBranch("mean_err");
    //auto avenergy = tree->GetBranch("avenergy");
    auto nevent = tree->GetEntries();

    Double_t mean;
    Double_t mean_err;
    Double_t avenergy;
    Int_t numcar;
    
    tree->SetBranchAddress("mean",&mean);
    tree->SetBranchAddress("mean_err",&mean_err);
    if ( type == 1 )
        tree->SetBranchAddress("avenergy",&avenergy);
    if ( type == 2 )
        tree->SetBranchAddress("numcar",&numcar);
    
    Double_t x[256];
    Double_t y[256];
    Double_t ye[256];

    Int_t N0 = 576; 
    Double_t energy_const = 0.13;
    //Double_t energy_const = 21.5/(1.27*0.001)/(2200*2200*N0*N0);
    for( Int_t i = 0; i < nevent; i++ )
    {
        tree->GetEntry(i);
        if ( type == 1 )
            x[i] = avenergy*energy_const;
        if ( type == 2 )
            x[i] = numcar;
        y[i] = mean;
        ye[i] = mean_err;
    }


    TGraphErrors* gre;

    TCanvas* c1;
    c1 = new TCanvas("c1","c1",1600,1200);

    /*
    TString sdraw;
    TString sedraw;
    TString sdrawto;
    TString sedrawto;

    sdraw.Form("mean:avenergy");
    sedraw.Form("mean_err:avenergy");
    sdrawto.Form(" >> h_mean(10)");
    sedrawto.Form(" >> h_meane(10)");
    

    tree->Draw(sdraw + sdrawto,"","goff");
    //tree->Draw(sdraw + sdrawto);
    TH2D* h_mean = (TH2D*) gDirectory->Get("h_mean");
    TGraph gr1(h_mean->ProfileX());

    tree->Draw(sedraw + sedrawto,"","goff");
    //tree->Draw(sedraw + sedrawto);
    TH2D* h_meane = (TH2D*) gDirectory->Get("h_meane");
    TGraph gr2(h_meane->ProfileX());
    
    */
    //TGraphErrors* gr = new TGraphErrors(10, gr1.GetX(), gr1.GetY(), NULL , gr2.GetY()); // Create graph with errors
    TGraphErrors* gr = new TGraphErrors(nevent,x,y, NULL , ye); // Create graph with errors

    gr->SetMarkerStyle(23);                 
    gr->SetMarkerColor(kBlue);                 
    gr->SetMarkerSize(2);
    gr->SetLineWidth(2);
    gr->SetTitle("");
//  gr->GetYaxis()->SetRangeUser(1700,1800);
    gr->GetYaxis()->SetRangeUser(0,range);
    gr->GetYaxis()->SetTitle("Solitons number");
    gr->GetYaxis()->SetTitleOffset(0.8);
    if ( type == 1 )
        gr->GetXaxis()->SetTitle("Value of L1 norm, ns mW^{1/2}");
    if ( type == 2 )
        gr->GetXaxis()->SetTitle("Number of subcarriers");
    gr->GetYaxis()->SetTitleSize(0.05);
    gr->GetXaxis()->SetTitleSize(0.05);
    gr->GetXaxis()->SetLabelSize(0.05);
    gr->GetYaxis()->SetLabelSize(0.05);
//  gr->SetMarkerColor(6);
    gr->Draw("APL");
    
    gPad->SetGrid(1,1);

    //gre = gr;
    //gre->Draw("APL*");
   
    c1->Print(output); 

    file->Close();
    
    return 0;
}

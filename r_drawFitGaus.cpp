#include <fstream>
#include <iostream>
#include "stdlib.h"

void r_drawFitGaus(TString dir, TString type, Int_t vartype, Int_t varind)
{
    /* Number of bins */
    Int_t NBIN = 140;
    
    Int_t NCAR = 16;
    Int_t NENG = 20;
    Int_t CYCLE = 0;
    //TString type("16QAM");

    Int_t ncar[16] = {16,32,48,64,80,96,112,128,144,160,176,192,208,224,240,256};
    Int_t energy[20] = {1,2,3,4,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80};
    
    Int_t cartype;
    Int_t energytype;

    std::stringstream rootname;
    rootname << dir << "16QAM_16_40.root" ;
    avenergy = 40;
    numcar = 16;
        
    fname = rootname.str();
    TFile* file = TFile::Open( fname.c_str() );
    TTree* tree; file->GetObject("tree;1",tree);

        
    TCanvas* c1;
    c1 = new TCanvas("c1","c1",1600,1200);
            
    TString sdraw;
    sdraw.Form("nsolit");
    TString sdrawto;
    sdrawto.Form(" >> h_res(%d,%d,%d)",NBIN,0,NBIN);

    tree->Draw(sdraw + sdrawto,"","goff"); //goff works? Stop draw in fitf
    TH1F* h_res = (TH1F*) gDirectory->Get("h_res");
        
    Double_t d_mean = h_res->GetMean();
         
    TF1* f_gaus = new TF1("f_gaus","gaus",0,NBIN);
    f_gaus->SetLineColor(kRed);
    f_gaus->SetLineWidth(4);
    f_gaus->SetParameter(1,4);
    f_gaus->SetParLimits(1,0.9*d_mean,1.1*d_mean);
    f_gaus->SetParameter(2,10);

    h_res->Fit("f_gaus","MQR0");

    Double_t par[3];
    f_gaus->GetParameters(&par[0]);
    chi2 = f_gaus->GetChisquare();

    Double_t err[3];
    err[0] = f_gaus->GetParError(0);
    err[1] = f_gaus->GetParError(1);
    err[2] = f_gaus->GetParError(2);
        
        
    /* ----------- */
        
    h_res->SetTitle("");
    h_res->SetLineColor(kBlue+3);
    h_res->SetFillColor(kBlue+2);
    h_res->SetFillStyle(3003);
    h_res->SetLineWidth(3);
    h_res->Draw();

    h_res->GetXaxis()->SetTitle("Solitons number");
    h_res->GetYaxis()->SetTitle("Count");
    h_res->GetYaxis()->SetTitleOffset(0.8);
    h_res->GetXaxis()->SetTitleSize(0.05);
    h_res->GetYaxis()->SetTitleSize(0.05);
        h_res->GetXaxis()->SetLabelSize(0.05);
        h_res->GetYaxis()->SetLabelSize(0.05);

        f_gaus->Draw("same");

        TLegend* leg = new TLegend(0.65,0.70,0.98,0.95);
    //  leg->SetHeader("Legend");
        //leg->SetBorderSize(0);
        leg->SetTextSize(.04);
        leg->AddEntry(h_res,"Distribution","f");
        leg->AddEntry("f_gaus","Gaussian fit","l");

        TString meanstr;
        meanstr.Form("Mean %.2f",mean);
        TString fwhmstr;
        Double_t fwhm = 2*sqrt(2*log(2))*sigma;
        fwhmstr.Form("FWHM %.2f",fwhm);
        
        leg->AddEntry((TObject*)0,meanstr,"");
        leg->AddEntry((TObject*)0,fwhmstr,"");
        leg->Draw("same");

        //TLegend* p662 = new TLegend(0.73,0.42,0.74,0.43);
        //p662->AddEntry((TObject*)0,"661.7 KeV","");
        //p662->SetBorderSize(0);
        //p662->SetTextSize(.05);
        //p662->Draw("same");

        /* ----------- */
        
       
        std::stringstream setname;
        //setname << dir << type << "_" << ncar[cartype] << "_" << energy[i];

        //setname.str();

        //TString setname;
        if( vartype == 1 )
        {
            setname << dir << type << "_" << ncar[cartype] << "_" << energy[i] << ".pdf";
            //setname.Form("../results_met_new/qpsk_%d_%i.pdf",ncar[cartype],energy[i]);
        }
        if( vartype == 2 )
        {
            setname << dir << type << "_" << ncar[i] << "_" << energy[energytype] << ".pdf";
            //setname.Form("../results_met_new/qpsk_%d_%i.pdf",ncar[i],energy[energytype]);
        }
        c1->Print(setname.str().c_str());
    
    return 0;
}

#!/bin/bash

# Sedov Egor
# egor.sedoff@gmail.com
# 08.02.2018

input_dir='../results_en_1/'
# type='16QAM'
max_ncar=256
max_energy=80
vartype=1

function doGetResultsD() {
    input='"'"${input_dir}${type}_${ncar}_${energy}.txt"'"'
    temp_type='"'$type'"'
    outdir='"'$input_dir'"'
    root -l -q "r_getResultsD.cpp($input,$outdir,$temp_type,$ncar,$energy)"
}

function doGetResults() {
    input='"'"${input_dir}${type}_${ncar}_${energy}.txt"'"'
    temp_type='"'$type'"'
    outdir='"'$input_dir'"'
    root -l -q "r_getResults.cpp($input,$outdir,$temp_type,$ncar,$energy)"
}

for type in 'QPSK'
do
if (( $vartype == 1 ))
then

    ncar=128
    for energy in 3 4 5 6 7
    do
        doGetResults
    done
    for energy in 3.2 3.4 3.6 3.8 4.2 4.4 4.6 4.8 5.2 5.4 5.6 5.8 6.2 6.4 6.6 6.8
    do
        doGetResultsD
    done

    ncar=1024
    for energy in 30 31 32 33 34 35 36 37 38 39 40 41 42
    do
        doGetResults
    done

elif (( $vartype == 2 ))
then
    for energy in 5 10 15 20 25 30 40
#    for energy in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192
    do
        ncar=16
        until (( $ncar > $max_ncar ))
        do
            doGetResults
            let "ncar = ncar + 16"
        done
    done
fi
done

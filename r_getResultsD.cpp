#include <fstream>
#include <iostream>
#include "stdlib.h"

void r_getResultsD(TString input, TString outdir, TString type, Int_t ncar, Double_t energy)
{

    std::ifstream fin;
    TString intxt(input);
    fin.open(intxt);

    std::stringstream rootname;
    rootname << outdir << type << "_" << ncar << "_" << energy <<".root" ;
    std::string fname = rootname.str();

    TFile* f = new TFile( fname.c_str(), "recreate" );
    TTree tree("tree","ROOT tree with results");

    Int_t nsolit;
    Double_t l1;
    Double_t l2;

    tree.Branch("type",&type);
    tree.Branch("ncar",&ncar,"ncar/I");
    tree.Branch("energy",&energy,"energy/D");

    tree.Branch("nsolit",&nsolit,"nsolit/I");
    tree.Branch("l1",&l1,"l1/D");
    tree.Branch("l2",&l2,"l2/D");

    char buff[50];

    while( !fin.eof() )
    {
        fin >> buff;
        nsolit = atoi(buff);
        fin >> buff;
        l1 = atof(buff);
        fin >> buff;
        l2 = atof(buff);
        
        if( !fin.eof() )
            tree.Fill();
    }

//    tree.Scan();

    tree.Write();
    f->Close();
    fin.close();
    

    return 0;
}
